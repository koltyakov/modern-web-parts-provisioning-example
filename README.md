# Modern Webparts Provisioning Sample

## Prerequisites

```powershell
powershell Install-Module SharePointPnPPowerShellOnline -SkipPublisherCheck -AllowClobber -Force
```

## Deploy

```powershell
powershell .\provision.ps1 -SiteUrl https://<tenant>.sharepoint.com/sites/<site>
```