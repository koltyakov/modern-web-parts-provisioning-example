Param(
  [Parameter(Mandatory = $true, Position = 1)]
  [string]$SiteUrl
)

Set-PnPTraceLog -on -level Debug;
$Credentials = Get-Credential -Message "Enter credentials to connect to $SiteUrl";
$Connection = Connect-PnPOnline -Url $SiteUrl -Credentials $Credentials -ReturnConnection;
Apply-PnPProvisioningTemplate -Path "$PSScriptRoot\template.xml" -Connection $Connection;